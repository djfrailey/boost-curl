<?php
/** 
* A simple curl wrapper for BoostMVC.
*
* @author David J. Frailey d.j.frailey@gmail.com
**/

namespace Boost;

boost()->add_callable('curl', 'Boost\Curl');

/**
* BoostMVC Curl Module
*
* CURL API Wrapper.
* 
* @todo Dev out XML parsing.
* @todo Dev out PUT method and file handling.
**/
class Curl extends Library
{

	/** @var resource|null $curl Holds the current curl handle. **/
	private $curl;
	/** @var string|null $error Contains the latest curl error. **/
	private $error;

	/**
	* Performs a CURL get operation.
	*
	* @param string $endpoint The URI to send the request to.
	* @param array $params A key-value pair of parameters to send along with the GET request.
	* @param array $options A key-value pair of CURLOPT constants to pass to curl_setopt_array, http://us2.php.net/manual/en/function.curl-setopt-array.php
	* @param boolean $return_array This value gets passed through to json_decode upon receiving a content type of application/json in the response headers.
	* @param boolean $error_report Setting this to true will enable CURLOPT_FAILONERROR.
	*
	* @return mixed Parses and returns the appropriate data depending on the content type of the response header. Returns false on failure.
	**/
	function get($endpoint,
				 $params = array(),
				 $options = array(),
				 $return_array = false,
				 $error_report = false)
	{
		$this->curl = curl_init();

		if (count($params) > 0)
		{
			$endpoint .= '?'.$this->parseParams($params);
		}

		if ($error_report)
			curl_setopt($this->curl, CURLOPT_FAILONERROR, true);

		curl_setopt($this->curl, CURLOPT_URL, $endpoint);
		curl_setopt($this->curl, CURLOPT_HTTPGET, true); // Force GET

		curl_setopt_array($this->curl, $options);
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);

		$data = curl_exec($this->curl);

		$success = $this->requestSuccess();
		$content_type = curl_getinfo($this->curl, CURLINFO_CONTENT_TYPE);

		curl_close($this->curl);

		if ($success !== false)
		{
			return $this->parseContent($data, $content_type, $return_array);
		}

		return false;
	}

	/**
	* Performs a CURL post operation.
	*
	* @param string $endpoint The URI to send the request to.
	* @param array $params A key-value pair of parameters to send along with the GET request.
	* @param array $options A key-value pair of CURLOPT constants to pass to curl_setopt_array, http://us2.php.net/manual/en/function.curl-setopt-array.php
	* @param boolean $return_array This value gets passed through to json_decode upon receiving a content type of application/json in the response headers.
	* @param boolean $error_report Setting this to true will enable CURLOPT_FAILONERROR.
	*
	* @return mixed Parses and returns the appropriate data depending on the content type of the response header. Returns false on failure.
	**/
	function post($endpoint,
				  $params = array(),
				  $options = array(),
				  $return_array = false,
				  $error_report = false)
	{

		$this->curl = curl_init();

		if (count($params) > 0)
		{
			$params = $this->parseParams($params);
		}

		if ($error_report)
			curl_setopt($this->curl, CURLOPT_FAILONERROR, true);

		curl_setopt($this->curl, CURLOPT_URL, $endpoint);
		curl_setopt($this->curl, CURLOPT_POST, true);

		curl_setopt_array($this->curl, $options);

		curl_setopt($this->curl, CURLOPT_POSTFIELDS, $params);
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);

		$data = curl_exec($this->curl);

		$success = $this->requestSuccess();
		$content_type = curl_getinfo($this->curl, CURLINFO_CONTENT_TYPE);

		curl_close($this->curl);

		if ($success !== false)
		{
			return $this->parseContent($data, $content_type, $return_array);
		}

		return false;
	}

	/**
	* Retreives the error stored in $error
	*
	* @return string|null
	**/
	function getLastError()
	{
		return $this->error;
	}

	/**
	* Checks the HTTP_CODE response header and ensures we aren't getting a response code of 400+
	*
	* @return boolean True upon success, False on failure.
	**/
	private function requestSuccess()
	{

		$info = curl_getinfo($this->curl);

		if ($info === false)
			return false;

		if ($info['http_code'] >= 400)
		{
			$this->error = curl_error($this->curl);
			return false;
		}

		return true;
	}

	/**
	* Creates an appropriate query string for both GET and POST requests.
	*
	* @param array $params A key value pair of parameters.
	*
	* @return string Returns an escaped query string.
	**/
	private function parseParams($params)
	{

		$rStr = '';

		if (!is_array($params))
			throw new InvalidArgumentException('CurlRequest::parseParams(): Parameter 1 must be an array.');
	
		$i = 0;
		foreach ($params as $param => $value)
		{
			$rStr .= $param.'='.urlencode($value);

			if ($i < (count($param) - 1))
				$rStr .= '&';

			$i++;
		}

		return $rStr;
	}

	/**
	* Determines the appropriate parsing operation based on the response headers content type.
	*
	* @param string $data The data to be parsed.
	* @param string $content_type The content type retreived from the response headers.
	* @param boolean $request_array Boolean value passed to json_decode.
	*
	* @return mixed <p>Returns the plain text data received. Content-Type: text/plain</p>
	* <p>Returns a stdObject or associative array depending on $request_array. Content-Type: application/json</p>
	* <p>Returns an associative array of parsed XML. Content-Type: application/xml, text/xml</p>
	**/
	private function parseContent($data, $content_type, $request_array = false)
	{

		if (strpos($content_type, ';', 0))
			$content_type = substr($content_type, 0, strpos($content_type, ';', 0));

		if ($content_type === 'text/plain')
		{
			return $data;
		}
		else if ($content_type === 'application/json')
		{

			$data = json_decode($data, $request_array);

			return $data;
		}
		else if ($content_type === 'application/xml' || $content_type === 'text/xml')
		{
			return $this->parseXML($data);
		}

		return false;
	}

	/**
	* Performs a CURL post operation.
	*
	* @param string $data The XML data to be parsed. 
	*
	* @todo Build this functionality.
	*
	* @return XMLNode returns an XMLNode Tree
	**/
	private function parseXML($data)
	{
		return boost()->xml->parse($data);
	}

}